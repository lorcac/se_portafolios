from django.contrib import admin
from .models import Mecanico, Reservadehora, Servicio, Tiposervicio

admin.site.register(Mecanico)
admin.site.register(Reservadehora)
admin.site.register(Servicio)
admin.site.register(Tiposervicio)
