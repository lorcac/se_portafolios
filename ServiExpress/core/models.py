# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models
from usuarios.models import Usuarioweb


class Mecanico(models.Model):
    id = models.BigAutoField(primary_key=True)
    nombre = models.CharField(max_length=100)
    telefono = models.IntegerField()
    email = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = True
        db_table = 'mecanico'


class Reservadehora(models.Model):
    id = models.BigAutoField(primary_key=True)
    fecha_solicitud = models.DateField()
    asunto = models.CharField(max_length=200)
    tiposervicio = models.ForeignKey('Tiposervicio', on_delete=models.CASCADE)
    usuario = models.ForeignKey(Usuarioweb, on_delete=models.CASCADE)

    class Meta:
        managed = True
        db_table = 'reservadehora'


class Servicio(models.Model):
    id = models.BigAutoField(primary_key=True)
    fecha = models.DateField()
    comentario = models.CharField(max_length=200, blank=True, null=True)
    validado = models.BooleanField(default=False)
    mecanico = models.ForeignKey(Mecanico, on_delete=models.CASCADE)
    reservadehora = models.ForeignKey(Reservadehora, on_delete=models.CASCADE)

    class Meta:
        managed = True
        db_table = 'servicio'


class Tiposervicio(models.Model):
    id = models.BigAutoField(primary_key=True)
    descripcion = models.CharField(max_length=100)
    precio = models.IntegerField()

    class Meta:
        managed = True
        db_table = 'tiposervicio'
