from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager

class UsuariowebManager(BaseUserManager):

    def create_user(self, email, username, nombres, apellido, password=None):
        if not email:
            raise ValueError("Email requerido.")
        if not username:
            raise ValueError("Usuario requerido.")
        if not nombres:
            raise ValueError("Debe ingresar su nombre.")
        if not apellido:
            raise ValueError("Debe ingresar su apellido.")
    
        user=self.model(
                email=self.normalize_email(email),
                username=username,
                nombres=nombres,
                apellido=apellido
            )

        user.set_password(password)
        user.save(using=self._db)
        return user
    
    def create_superuser(self, email, username, nombres, apellido, password):
        user    = self.create_user(
            email=self.normalize_email(email),
            password=password,
            username=username,
            nombres=nombres,
            apellido=apellido
        )
        user.is_admin = True
        user.is_staff = True
        user.superuser = True
        user.save(using=self._db)
        return user

class Usuarioweb(AbstractBaseUser):
    email               = models.EmailField(verbose_name='email', max_length=150, unique=True)
    username            = models.CharField(max_length=50, unique=True)
    date_joined         = models.DateTimeField(verbose_name='fecha de ingreso', auto_now_add=True)
    last_login          = models.DateTimeField(verbose_name='ultimo inicio de sesion', auto_now=True)
    is_admin            = models.BooleanField(default=False)
    is_active           = models.BooleanField(default=True)
    is_staff            = models.BooleanField(default=False)
    is_superuser        = models.BooleanField(default=False)
    nombres             = models.CharField(max_length=200)
    apellido            = models.CharField(max_length=150)
    fecha_nacimiento    = models.DateField(verbose_name='fecha de nacimiento', null=True, blank=True)

    objects = UsuariowebManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username', 'nombres','apellido',]

    def __str__(self):
        return self.email
    
    def has_perm(self, perm, obj=None):
        return self.is_admin
    
    def has_module_perms(self, app_label):
        return True

